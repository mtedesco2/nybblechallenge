import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WeatherWidgetComponent } from './weather-widget.component';
import { WeatherService } from 'src/app/core/services/weather.service';
import { WeatherForecast } from './weather-widget.model';
import { HttpClientModule } from '@angular/common/http';
import { of } from 'rxjs';

describe('WeatherWidgetComponent', () => {
  let component: WeatherWidgetComponent;
  let fixture: ComponentFixture<WeatherWidgetComponent>;
  let service;

  const forecast: WeatherForecast[] = [
    {
      date: new Date(),
      icon: 'icon',
      minTemperature: 1,
      maxTemperature: 2
    },
    {
      date: new Date(),
      icon: 'icon',
      minTemperature: 1,
      maxTemperature: 2
    },
    {
      date: new Date(),
      icon: 'icon',
      minTemperature: 1,
      maxTemperature: 2
    },
    {
      date: new Date(),
      icon: 'icon',
      minTemperature: 1,
      maxTemperature: 2
    }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WeatherWidgetComponent],
      imports: [HttpClientModule],
      providers: [WeatherService]
    }).compileComponents();
  }));

  beforeEach(async () => {
    fixture = TestBed.createComponent(WeatherWidgetComponent);
    component = fixture.componentInstance;
    service = TestBed.get(WeatherService);
    spyOn(service, 'getWeather').and.returnValue(of(forecast));
    fixture.detectChanges();
  });

  it('should get forecast information', () => {
    expect(component.weatherForecast).toBe(forecast);
  });
});
