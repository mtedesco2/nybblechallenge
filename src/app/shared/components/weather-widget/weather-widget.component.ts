import { Component, OnInit } from '@angular/core';
import { WeatherService } from 'src/app/core/services/weather.service';
import { WeatherForecast } from './weather-widget.model';

@Component({
  selector: 'app-weather-widget',
  templateUrl: './weather-widget.component.html',
  styleUrls: ['./weather-widget.component.scss']
})
export class WeatherWidgetComponent implements OnInit {
  constructor(private weatherService: WeatherService) {}

  weatherForecast: WeatherForecast[];

  ngOnInit() {
    this.weatherService.getWeather().subscribe(response => {
      this.weatherForecast = response;
    });
  }
}
