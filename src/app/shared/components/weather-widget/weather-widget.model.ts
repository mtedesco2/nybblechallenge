export interface WeatherForecast {
  date: Date;
  icon: string;
  minTemperature: number;
  maxTemperature: number;
}
