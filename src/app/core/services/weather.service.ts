import { Injectable } from '@angular/core';
import { catchError, concatMap, tap, map } from 'rxjs/operators';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  apiKey: string = '2a958c2e4bc230a3cdf5593c5e1d157e';
  darkSkyUrl: string =
    'https://cors-anywhere.herokuapp.com/https://api.darksky.net/forecast';

  constructor(private httpClient: HttpClient) {}

  private getLocation(): Observable<any> {
    return Observable.create(observer => {
      if (window.navigator && window.navigator.geolocation) {
        window.navigator.geolocation.getCurrentPosition(
          position => {
            observer.next(position);
            observer.complete();
          },
          error => observer.error(error)
        );
      } else {
        observer.error('Unsupported Browser');
      }
    });
  }

  private getWeatherByLocation(latitude: string, longitude: string) {
    return this.httpClient
      .get<any>(
        `${this.darkSkyUrl}/${this.apiKey}/${latitude},${longitude}?exclude=currently,flags,minutely,hourly`
      )
      .pipe(
        catchError((error: any) =>
          observableThrowError(error.error || 'Server error')
        )
      );
  }

  getWeather() {
    return this.getLocation()
      .pipe(
        concatMap(locationResponse =>
          this.getWeatherByLocation(
            locationResponse.coords.latitude,
            locationResponse.coords.longitude
          )
        )
      )
      .pipe(
        map(response => {
          return response.daily.data.map(weather => {
            return {
              date: new Date(weather.time * 1000),
              icon: weather.icon,
              minTemperature: this.fahrenheitToCelcius(weather.temperatureMin),
              maxTemperature: this.fahrenheitToCelcius(weather.temperatureMax)
            };
          });
        })
      );
  }

  private fahrenheitToCelcius(fah) {
    return ((5.0 / 9.0) * (fah - 32)).toFixed(2);
  }
}
