import { TestBed, getTestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { InvoiceService } from './invoice.service';

describe('InvoiceService', () => {
  let injector: TestBed;
  let service: InvoiceService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [InvoiceService]
    });

    injector = getTestBed();
    service = injector.get(InvoiceService);
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('it should return an invoice number', () => {
    const number = service.getInvoiceNumber();
    expect(number).not.toEqual(null);
  });

  it('it should return saved data', () => {
    const invoices: string = JSON.stringify([
      {
        number: 'XXX0000009',
        net: 1000,
        tax: 500,
        total: 1500,
        percetangeTax: 500
      }
    ]);
    spyOn(localStorage, 'getItem').and.callFake(key => invoices);
    const result = service.getSavedData();
    expect(result).toEqual(result);
  });
});
