import { TestBed, getTestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { WeatherService } from './weather.service';
import { of } from 'rxjs';

describe('WeatherService', () => {
  let injector: TestBed;
  let service: WeatherService;
  let httpMock: HttpTestingController;

  const forecast = [
    {
      date: new Date(),
      icon: 'icon',
      minTemperature: 1,
      maxTemperature: 2
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [WeatherService]
    });

    injector = getTestBed();
    service = injector.get(WeatherService);
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('it should call weather api', () => {
    const weatherResult = service.getWeather();
    expect(weatherResult).not.toEqual(null);
  });
});
