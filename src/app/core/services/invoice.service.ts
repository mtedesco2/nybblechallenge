import { Injectable } from '@angular/core';
import { Invoice } from 'src/app/features/invoices/invoice.model';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {
  invoiceNumber: number = 0;

  constructor() {
    const invoice = localStorage.getItem('invoiceNumber');
    this.invoiceNumber = invoice !== null ? Number(invoice) : 0;
  }

  saveInvoices(invoices: Invoice[]) {
    localStorage.setItem('invoiceNumber', (this.invoiceNumber - 1).toString());
    localStorage.setItem('savedData', JSON.stringify(invoices));
  }

  getSavedData(): Invoice[] {
    const invoices = JSON.parse(localStorage.getItem('savedData'));
    return invoices === null ? [] : invoices;
  }

  getInvoiceNumber(): string {
    this.invoiceNumber++;
    const value = this.invoiceNumber.toString();
    return value.length < 10 ? this.pad('0' + value) : value;
  }

  deleteWork() {
    localStorage.clear();
  }

  private pad(value): string {
    value = value.toString();
    return value.length < 10 ? this.pad('0' + value) : value;
  }
}
