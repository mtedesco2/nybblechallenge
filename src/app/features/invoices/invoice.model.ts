export interface Invoice {
  number: string;
  net: number;
  percetangeTax: number;
  tax: number;
  total: number;
}
