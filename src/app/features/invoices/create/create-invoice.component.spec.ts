import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WeatherService } from 'src/app/core/services/weather.service';
import { HttpClientModule } from '@angular/common/http';
import { of } from 'rxjs';
import { CreateInvoiceComponent } from './create-invoice.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import localeEsAr from '@angular/common/locales/es-AR';
import { registerLocaleData } from '@angular/common';
import { Router } from '@angular/router';
registerLocaleData(localeEsAr);

describe('CreateInvoiceComponent', () => {
  let component: CreateInvoiceComponent;
  let fixture: ComponentFixture<CreateInvoiceComponent>;
  let service;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateInvoiceComponent],
      imports: [
        ReactiveFormsModule,
        HttpClientModule,
        RouterTestingModule.withRoutes([])
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(async () => {
    fixture = TestBed.createComponent(CreateInvoiceComponent);
    component = fixture.componentInstance;
    service = TestBed.get(WeatherService);
    fixture.detectChanges();
  });

  it('should add an invoice', () => {
    component.form.controls.net.setValue(1000);
    component.form.controls.tax.setValue(1000);
    component.form.controls.percentageTax.setValue('21%');
    component.onChangeTax();
    component.onAdd();
    expect(component.invoices.length).toBeGreaterThan(0);
  });

  it('should remove an invoice', () => {
    component.form.controls.net.setValue(1000);
    component.form.controls.tax.setValue(1000);
    component.form.controls.percentageTax.setValue('21%');
    component.onChangeTax();
    component.onAdd();
    const invoices = component.invoices.length;
    component.onRemove(0);
    expect(component.invoices.length).toEqual(invoices - 1);
  });
});
