import { Component } from '@angular/core';
import { Invoice } from '../invoice.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { InvoiceService } from '../../../core/services/invoice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-invoice',
  templateUrl: './create-invoice.component.html',
  styleUrls: ['./create-invoice.component.scss'],
  providers: []
})
export class CreateInvoiceComponent {
  invoices: Invoice[] = [];
  percentageTaxes = ['0%', '10.5%', '21%', '27%'];
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private invoiceService: InvoiceService,
    private router: Router
  ) {
    this.form = this.formBuilder.group({
      number: invoiceService.getInvoiceNumber(),
      net: [null, Validators.required],
      tax: [null, Validators.required],
      percentageTax: null,
      total: null
    });
    this.invoices = this.invoiceService.getSavedData();
  }

  onAdd() {
    const net = this.form.controls['net'].value;
    const tax = this.getTax();

    if (net !== null && tax !== null) {
      const invoice: Invoice = {
        number: this.form.controls['number'].value,
        net: this.form.controls['net'].value,
        tax: tax,
        total: this.form.controls['total'].value,
        percetangeTax: this.form.controls['percentageTax'].value
      };
      this.invoices.push(invoice);
      this.clearForm();
      this.setInvoiceNumber();
      this.invoiceService.saveInvoices(this.invoices);
    }
  }

  onClear() {
    this.clearForm();
  }

  onRemove(index) {
    this.invoices.splice(index, 1);
    this.invoiceService.saveInvoices(this.invoices);
  }

  onChangeTax() {
    this.calculateTotal();
  }

  onChangeNet() {
    this.calculateTotal();
  }

  onProcess() {
      if (this.invoices.length > 0) {
      this.router.navigateByUrl('invoices/summary');
    }
  }

  private calculateTotal() {
    const net = this.form.controls['net'].value;
    const tax = this.getTax();

    if (net !== null && tax !== null) {
      const total = (net + tax).toFixed(2);
      this.form.controls['total'].setValue(total);
    }
  }

  private getTax() {
    const net = this.form.controls['net'].value;
    const percetangeTax = this.form.controls['percentageTax'].value;

    if (net !== null && percetangeTax !== null) {
      const percentage = parseFloat(
        this.form.controls['percentageTax'].value
      ).toFixed(2);

      return (net * Number(percentage)) / 100;
    } else {
      return null;
    }
  }

  private clearForm() {
    this.form.controls['net'].setValue(null);
    this.form.controls['tax'].setValue(null);
    this.form.controls['total'].setValue(null);
  }

  private setInvoiceNumber() {
    this.form.controls['number'].setValue(
      this.invoiceService.getInvoiceNumber()
    );
  }
}
