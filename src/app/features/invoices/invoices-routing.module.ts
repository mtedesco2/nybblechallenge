import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { SummaryInvoiceComponent } from './summary/summary-invoice.component';
import { CreateInvoiceComponent } from './create/create-invoice.component';

const invoiceRoutes: Routes = [
  {
    path: '',
    component: CreateInvoiceComponent,
  },
  {
    path: 'summary',
    component: SummaryInvoiceComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(invoiceRoutes)],
  declarations: [],
  exports: [RouterModule]
})
export class InvoiceRoutingModule {}
