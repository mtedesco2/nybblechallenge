import { NgModule } from '@angular/core';
import { InvoiceRoutingModule } from './invoices-routing.module';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { SummaryInvoiceComponent } from './summary/summary-invoice.component';
import { CreateInvoiceComponent } from './create/create-invoice.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [CreateInvoiceComponent, SummaryInvoiceComponent],
  imports: [
    InvoiceRoutingModule,
    CommonModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class InvoiceModule {}
