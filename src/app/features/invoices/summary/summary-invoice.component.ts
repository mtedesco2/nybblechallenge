import { Component } from '@angular/core';
import { Invoice } from '../invoice.model';
import { InvoiceService } from '../../../core/services/invoice.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-summary-invoice',
  templateUrl: './summary-invoice.component.html',
  styleUrls: ['./summary-invoice.component.scss'],
  providers: []
})
export class SummaryInvoiceComponent {
  invoices: Invoice[] = [];

  constructor(private invoiceService: InvoiceService, private router: Router) {
    this.invoices = this.invoiceService.getSavedData();
  }

  onDeleteWork() {
    this.invoiceService.deleteWork();
    this.router.navigateByUrl('invoices');
  }
}
